package employees

import java.time.LocalDate

/**
 *This class should have:
 *firstName
 *lastName
 * department
 * hiringDate - cannot be modified
 */

abstract class Employee (
    override var firstName: String,
    override var lastName : String,
    var department : String,
    val hiringDate : LocalDate,
    var location: LocationDetails,
    ) : EmployeeInterface {

    var myID: String = ""
    override fun doSomething(): String = "Sample String"

    fun myEmployee() = "Employee Name: $firstName from $department department."

    init {
        this.myID = ("${firstName.substring(0,2).uppercase()} - ${
            lastName.takeLast(2).uppercase()
        } - ${(10000000 until 99999999).random()}")
    }
}

class RankAndFileEmployee (
    firstName: String,
    lastName: String,
    department: String,
    hiringDate: LocalDate,
    location: LocationDetails
    ): Employee(
    firstName,
    lastName,
    department,
    hiringDate,
    location
    ) {
    val isRankAndFile = true
    val isAdmin = false
}

class AdminEmployee(
    firstName: String,
    lastName: String,
    department: String,
    hiringDate: LocalDate,
    location: LocationDetails
): Employee(
    firstName,
    lastName,
    department,
    hiringDate,
    location
){
    val isRankAndFile = false
    val isAdmin = true
}

//class containing all the details about the office location of an employee
//City, Province, Region, Exact Address

//Data Class -> Automatic string conversion
// Equal() checks the actual data
// Can use Copy()
data class LocationDetails(
    var city: String,
    var province: String,
    var region: String,
    var exactAddress:String
) {

}
