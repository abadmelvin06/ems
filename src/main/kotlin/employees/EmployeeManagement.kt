package employees

import jdk.incubator.foreign.Addressable
import java.time.LocalDate

fun main(){
    // val employee1 = RankAndFileEmployee("Amy", "Carter", "IT", LocalDate.now())
    // val employee2 = AdminEmployee("Brandon","Cruz", "HR", LocalDate.now())

//    with(employee2){
//        println(firstName)
//        println(lastName)
//        println(myID)
//        println(department)
//        println(isRankAndFile)
//        println(isAdmin)
//    }
//    employee2.apply {
//        this.firstName = firstName.uppercase()
//    }
//    println(employee2.firstName)

    val address1 = LocationDetails(
        "Makati",
        "Metro Manila",
        "NCR",
        "1124 XYZ Building, Legaspi Village"
    )

    val address2 = address1.copy(
        exactAddress = "1623 JHK Building, Salcedo Village"
    )

    val employee3 = RankAndFileEmployee("Jeff", "Castro", "IT", LocalDate.now(), address1)
    println(employee3.location)
}